# EnBuscaDeLaFelicidad Versión 1

## Autor

### Teymuraz Datebashvili

```[Propiedades CSS]
@font-face
display: flex;
flex-flow: column nowrap;
justify-content: space-around;
align-items: center;
border: 5px outset grey;
padding: 5px;
background-color: rgb(0, 0, 0);
font-size: 0px;
border-radius: 0px;
width: 0px;
position: relative;
display: inline-block;
position: absolute;
box-shadow: 0px rgba(0,0,0);
z-index: 1;
text-decoration: underline;
a:hover {background-color: #ddd;}
.dropdown:hover {display: block;}
.dropdown:hover {background-color: #cf2b16;}
text-align: center;
font-family: 'Allerta Stencil', sans-serif;
text-decoration: underline;
border-collapse: collapse;
```

*Enlaces que he utilizado*
[W3schools](URL "https://www.w3schools.com/")
[Dafont](URL "https://www.dafont.com/es/")
[Clonezilla](URL "http://www.colorzilla.com/gradient-editor/")
[FlexBox](URL "https://css-tricks.com/snippets/css/a-guide-to-flexbox/")